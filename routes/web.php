<?php

// Route::get('/', function () {
//     return view('site.home.index');
// });

//Site
Route::get('/', 'SitesController@index');

// Buscar anúncios
Route::any('buscar-anuncios', 'SitesController@searchPublicAdvertisements')->name('anuncios.searchPublicAdvertisements');

// Promoções
Route::get('promocoes', 'SitesController@promotion')->name('promocoes.promotion');

// Sobre
Route::get('sobre', 'SitesController@about')->name('sobre.about');

// Contato
Route::get('contato', 'ContactsController@contact')->name('contato.contact');
Route::post('contato', 'ContactsController@store')->name('contato.store');

// Assinar
Route::get('assine', 'SitesController@sign')->name('assine.sign');

Route::prefix('admin')
	->middleware(['auth'])
	->group(function(){

		//acessando advertisements controller
		Route::resource('anuncios', 'AdvertisementsController');

		//acessando promotions controller
		Route::resource('promocoes', 'PromotionsController');

		// any aceita qualquer tipo de requisicao, post, get , put etc...
		Route::any('anuncios-buscar', 'AdvertisementsController@searchAdminAdvertisements')->name('anuncios.searchAdminAdvertisements');
		
		//Advertisements
		Route::post('anuncios/criar', 'AdvertisementsController@store')->name('anuncios.store');
		Route::get('meu-perfil', 'UsersController@profile')->name('profile');
		Route::post('atualizar-perfil', 'UsersController@profileUpdate')->name('profileUpdate');

		//Promotions
		Route::post('promocoes/criar', 'PromotionsController@store')->name('promocoes.store');
		Route::any('promocoes/publicar{id}', 'PromotionsController@publish')->name('promocoes.publish');
		Route::any('promocoes/despublicar{id}', 'PromotionsController@unpublish')->name('promocoes.unpublish');
	});

Auth::routes();