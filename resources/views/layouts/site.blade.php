<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gear List | Criação de anúncios e promoções em formato de uma lista telefônica online</title>
    <meta name="keywords" content="gear list, gear lista, lista, list, lista online, online list, lista telefônica online, anúncios online, promoções online, chacara, chacaras, chácara, chácaras, lanchonetes, mecanica, mecânica, mecanicas, mecânicas, mercado, mercados, supermercado, supermercados, telefone, telefones, endereço, endereços">
    <meta name="description" content="Gear List - Crie anúncios e promoções com muita facilidade e praticidade e os personalize da forma que desejar! Inove e atraia novos clientes para seu comércio todos os dias.">
    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Gear List | Criação de anúncios e promoções em formato de uma lista telefônica online">
    <meta property="og:description" content="Gear List - Crie anúncios e promoções com muita facilidade e praticidade e os personalize da forma que desejar! Inove e atraia novos clientes para seu comércio todos os dias.">
    <meta property="og:site_name" content="Gear List">
    <meta property="og:url" content="https://www.gearlist.com.br/">
    <meta name="google-site-verification" content="j2xYTEYNQ7eQaYaRjMa7id9FMXINJRvz6QVBfMbizTw" />
    <!-- Fontes -->
    <link href="https://fonts.googleapis.com/css?family=Istok+Web" rel="stylesheet">
    <!-- Estilos -->
    <link rel="shortcut icon" href="images/gearlist-favicon.png">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/css/style.css')}} ">
    <link rel="stylesheet" href="{{ asset('/css/hover.css')}} ">
    <link rel="stylesheet" href="{{ asset('/css/pic.css')}} ">
</head>
<body>

    <!-- include navbar -->
    @include('site.includes.nav')
    <!-- fim da chamada -->

    <form action="{{ route('anuncios.searchPublicAdvertisements') }}" method="POST" class="form form-inline searchAdvt" id="searchAdvt" style="margin-top:50px;">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="post">
        <div class="row col-md-12">
            <div class="col-md-4 offset-md-1 text-center">
                <label>O que você procura?</label>
                <select name="category" id="search-category" class="btn btn-primary col-md-12 form-control search-category">
                    <option value="">Todas as Categorias</option>
                    @foreach ($categories as $key => $category)
                        <option value="{{ $key }}">{{ $category }}</option>
                    @endforeach
                </select>
            </div>
            <div style="margin-top:10px;" class="col-md-4">
                <div class="search-bar">
                    <input type="text" name="descricao" class="col-md-12" placeholder="Pesquisar" style="height:50px;">
                </div>
            </div>
            <div class="search-button">
                <a href="#" class="btn btn-primary" onClick="document.getElementById('searchAdvt').submit();"><i class="fas fa-2x fa-search"></i></a>
            </div>
        </div>
    </form><br>

    <div id="page-content">
        @yield('content')
    </div>

    <!-- include footer -->
    @include('site.includes.footer')
    <!-- fim da chamada -->

</body>
<!-- Scripts -->
<script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- submitando form de categorias ao selecionar no select sem precisar clicar na lupa -->
<script>
    $(document).ready(function() {
        $('.search-category').on('change', function() {
            $('.searchAdvt').submit();
        });
    });
</script>
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
</html>