@extends('layouts.site')

@section('content')
<div id="" class="container">
    <div class="col-md-12">
        <div style="margin-top: 25px; margin-bottom: 50px;" class="row">
            <div style="margin-top: 50px;" class="col-md-4">
                <div class="card hvr-float-shadow">
                    <img class="card-img-top" src="images/lanchonetes.jpg" alt="restaurantes">
                    <div class="card-body">
                        <h5 class="card-title">Lanchonetes</h5>
                        <a href="{{('buscar-anuncios?category=LAN')}}" class="btn btn-primary">Ver Todas</a>
                    </div>
                </div>
            </div>
            <div style="margin-top: 50px;" class="col-md-4">
                <div class="card hvr-float-shadow">
                    <img class="card-img-top" src="images/chacaras.jpg" alt="chacaras">
                    <div class="card-body">
                        <h5 class="card-title">Chácaras</h5>
                        <a href="{{('buscar-anuncios?category=CHA')}}" class="btn btn-primary">Ver Todas</a>
                    </div>
                </div>
            </div>
            <div style="margin-top: 50px;" class="col-md-4">
                <div class="card hvr-float-shadow">
                    <img class="card-img-top" src="images/mecanicas.jpg" alt="mecanicas">
                    <div class="card-body">
                        <h5 class="card-title">Mecânicas</h5>
                        <a href="{{('buscar-anuncios?category=MEC')}}" class="btn btn-primary">Ver Todas</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div style="margin-top: 25px; margin-bottom: 50px;" class="row">
            <div style="margin-top: 50px;" class="col-md-4">
                <div class="card hvr-float-shadow">
                    <img class="card-img-top" src="images/supermercados.jpg" alt="supermercados">
                    <div class="card-body">
                        <h5 class="card-title">Supermercados</h5>
                        <a href="{{('buscar-anuncios?category=SUP')}}" class="btn btn-primary">Ver Todos</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection