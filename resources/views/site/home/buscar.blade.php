@extends('layouts.site')

@section('content')

<div style="color: #fff;" class="col-md-10 offset-md-2">
    <p class="text-center">{{$msg}}</p>
    <div style="margin-top:100px;" class="search-content row">
        @foreach($advertisements as $advertisement)

                <!-- chamando modal para poder fazer iteracao das imagens no carousel -->
                @include('site.includes.modal')
                <!-- fim da chamada -->

                <div class="background-advertisement col-md-3">
                    <img class="col-md-12 img-responsive" src="/images/advertisements-images/{{$advertisement->advertisement_images->first->image['image']}}" alt="">
                    <p style="word-wrap: break-word; margin-top:10px;">{{$advertisement->owner_name }}</p>
                    @if($advertisement->category == 'CHA')
                        <span><i class="fas fa-bed"></i> {{ $advertisement->room }} Quartos</span>
                        <br><span><i class="fas fa-users"></i> {{ $advertisement->capacity }} Acomodações</span>
                        <br><span><i class="fas fa-dollar-sign"></i> Valor da diária: {{ 'R$ '.number_format($advertisement->diary_price, 2, ',', '.') }}</span>
                    @endif
                    @if($advertisement->category == 'MEC')
                        <span><i class="fas fa-wrench"></i> Especialidades da mecânica:</span>
                        @foreach($advertisement->mechanic_parts as $key => $mechanic)
                            <br><span> - {{$mechanic->mechanic_part}}</span>
                        @endforeach
                    @endif
                    @if($advertisement->category == 'LAN')
                        <span><i class="fas fa-utensils"></i> Opções variadas:</span>
                        @foreach($advertisement->sandwich_variations as $key => $sandwich)
                            <br><span> - {{$sandwich->sandwich_variation}}</span>
                        @endforeach
                    @endif
                    <div style="border-top: 1px solid rgba(0,0,0,.1); margin-top:15px; margin-left:-15px; margin-right:-15px; padding-top:5px; padding-bottom:5px;">
                        <button type="button" data-toggle="modal" data-target="#advt-extra-info_{{$advertisement->id}}" style="margin:5px; float:right;" class="btn btn-primary">Saiba <span style="color:#fff !important;" class="fas fa-plus"></span></button>
                    </div>
                </div>
        @endforeach
    </div>
    <div style="margin-top:50px;" class="offset-md-5">
        <!-- paginacao -->
        @if (isset($dataForm))
            <!-- faz com que os parametros do filtro sejam mantidos ate quando paginar -->    
            {!! $advertisements->appends($dataForm)->links() !!}
        @else
            {!! $advertisements->links() !!}
        @endif
    </div>
</div>
@endsection