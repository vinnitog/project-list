@extends('layouts.promotion')

@section('content')
<div class="col-md-10 offset-md-2 text-center">
    <div class="row">
        @foreach($promotions as $promotion)
            <div class="promotional-itens col-md-3">
                <p style="margin-top: 10px; word-wrap: break-word;" class="text-center"><i class="fa fa-2x fa-certificate"></i> {{$promotion->owner_name}}</p><br>
                <img style="border-radius: 5px;" class="col-md-10" src="images/promotions-images/{{$promotion->image}}" alt="imagem"><br>
                <p style="margin-top: 10px; word-wrap: break-word;">{{$promotion->name}}</p><br>
                <img class="user-promotion-image" src="images/users-images/{{$promotion->user->image}}" alt="{{$promotion->user->name}}">
                <p>{{$promotion->user->name}}</p>
                <span><i class="fas fa-2x fa-phone-square"></i> {{$promotion->phone }}</span>
                <span><i class="fas fa-2x fa-dollar-sign"></i> {{ 'R$ '.number_format($promotion->price, 2, ',', '.') }}</span>
            </div>
        @endforeach
    </div>
    <div style="margin-top:50px;" class="offset-md-5">
        <!-- paginacao -->
        {!! $promotions->links() !!}
    </div>
</div>
@endsection