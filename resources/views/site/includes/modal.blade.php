<!-- Modal para ver mais informações dos anuncios -->
<div class="modal fade" id="advt-extra-info_{{$advertisement->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="color: #333;" class="modal-title" id="exampleModalLabel">Informações Extras</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div id="demo_{{$advertisement->id}}" class="carousel slide" data-ride="carousel">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        @foreach($advertisement->advertisement_images as $images)
                            <li data-target="#demo_{{$advertisement->id}}" data-slide-to="{{$images->id}}" class="{{$images->first ? 'active' : ''}}"></li>
                        @endforeach
                    </ul>
                    
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="col-md-5" src="/images/advertisements-images/{{$advertisement->advertisement_images->first->image['image']}}" alt="">
                        </div>
                        <!-- necessario o foreach comecar da segunda imagem pois a primeira esta dentro do carousel active para funcionar a animacao -->
                        @foreach($advertisement->advertisement_images as $key => $images)
                        @if($key > 0)
                            <div class="carousel-item">
                                <img class="col-md-6" src="/images/advertisements-images/{{$images->image}}" alt="">
                            </div>
                        @endif
                        @endforeach
                    </div>
                    
                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo_{{$advertisement->id}}" data-slide="prev">
                        <span<i style="color:#333;" class="fas fa-2x fa-chevron-left"></i></span>
                    </a>
                    <a class="carousel-control-next" href="#demo_{{$advertisement->id}}" data-slide="next">
                        <span><i style="color:#333;" class="fas fa-2x fa-chevron-right"></i></span>
                    </a>
                </div>
                <div style="margin-top:100px;">
                    <p style="text-align: left !important;"><b>Descrição:</b></p>
                    <p style="text-align: justify; word-wrap: break-word;" >{{$advertisement->description}}</p>
                    <br><p style="text-align: left !important;"><b>Dados de contato:</b></p>
                    <p style="text-align: left !important;"><i class="fas fa-phone-square"></i> {{$advertisement->phone }}</p>
                    <p style="text-align: left !important;">
                        @if(strpos($advertisement->social_media,'facebook') == true)
                            <a style="color: #333 !important;" href="{{$advertisement->social_media }}" target="_blank"><i style="color: #333 !important;" class="fab fa-facebook"></i> Facebook</a>
                        @else
                            <a style="color: #333 !important;" href="{{$advertisement->social_media }}" target="_blank"><i style="color: #333 !important;" class="fab fa-instagram"></i> Instagram</a>
                        @endif
                    </p>
                    @if($advertisement->email)
                        <p style="text-align: left !important;"><i class="far fa-envelope"></i> {{ $advertisement->email }} </p>
                    @endif
                </div>
                <br><p style="text-align: left !important;"><b>Demais informações:</b></p>
                @if($advertisement->category == 'MEC')
                    <div class="col-md-12 text-center">
                        <div class="extra-infos row">
                            <div class="col-md-4">
                                <label><b>Horário de Funcionamento:</b></label>
                                <span>Abre ás: {{ date('H:i', strtotime($advertisement->open_time)) }}</span>
                                <span>- Fecha ás: {{ date('H:i', strtotime($advertisement->close_time)) }}</span>
                            </div>
                        </div>
                    </div>
                @endif
                @if($advertisement->category == 'LAN' || $advertisement->category == 'SUP')
                    <div class="col-md-12 text-center">
                        <div class="extra-infos row">
                            <div class="col-md-4">
                                <label><b>Horário de Funcionamento:</b></label>
                                <span>Abre ás: {{ date('H:i', strtotime($advertisement->open_time)) }}</span>
                                <span>- Fecha ás: {{ date('H:i', strtotime($advertisement->close_time)) }}</span>
                            </div>
                            <div class="col-md-4">
                                <label><b>Delivery:</b></label>
                                @if($advertisement->delivery == 1)
                                    <span>{{ $advertisement->delivery = "SIM"}}</span>
                                @else
                                    <span>{{ $advertisement->delivery = "NÃO"}}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
                @if($advertisement->category == 'CHA')
                    <div class="col-md-12">
                        <div class="extra-infos row">
                            <div class="col-md-4">
                                <label><b>Áreas de Lazer:</b></label>
                                    @foreach($advertisement->recreation_areas as $key => $recreation)
                                        <br><span>{{$recreation->recreation_area}}</span>
                                    @endforeach
                            </div>
                            <div class="col-md-4">
                                <label><b>Itens Extras:</b></label>
                                    @foreach($advertisement->extra_items as $key => $extra)
                                        <br><span>{{$extra->extra_item}}</span>
                                    @endforeach
                            </div>
                            <div class="col-md-4">
                                <label><b>Alugada no Feriado:</b></label>
                                @if($advertisement->holiday == 1)
                                    <br><span>{{ $advertisement->holiday = "SIM"}}</span>
                                @else
                                    <br><span>{{ $advertisement->holiday = "NÃO"}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="extra-infos row">
                            <div class="col-md-4">
                                <label><b>Alugada no Natal:</b></label>
                                @if($advertisement->christmas == 1)
                                    <br><span>{{ $advertisement->christmas = "SIM"}}</span>
                                @else
                                    <br><span>{{ $advertisement->christmas = "NÃO"}}</span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <label><b>Alugada no Ano Novo:</b></label>
                                @if($advertisement->new_year == 1)
                                    <br><span>{{ $advertisement->new_year = "SIM"}}</span>
                                @else
                                    <br><span>{{ $advertisement->new_year = "NÃO"}}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-defautl" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>