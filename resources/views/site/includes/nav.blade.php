<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbar-scroll-animation">
    <a style="margin-top:15px;" class="navbar-brand" href="/">
        <p><i class="fas fa-cogs"></i> GearList</p>
        <!-- <img class="img-responsive" src="images/logo.png" alt="logo"> -->
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a style="margin-top:15px;" href="{{route('promocoes.promotion')}}" class="nav-link">
                    <p>Promoções</p>
                </a>
            </li>
            <li class="nav-item">
                <a style="margin-top:15px;" href="{{route('sobre.about')}}" class="nav-link">
                    <p>Sobre</p>
                </a>
            </li>
            <li class="nav-item">
                <a style="margin-top:15px;" href="{{route('contato.contact')}}" class="nav-link">
                    <p>Contato</p>
                </a>
            </li>
            <li class="nav-item">
                <a style="margin-top:15px;" href="{{route('assine.sign')}}" class="nav-link">
                    <p>Asssine</p>
                </a>
            </li>
        </ul>
        @if (Route::has('login'))
            <ul class="navbar-nav ml-auto">
                @auth
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/admin/anuncios') }}"><i class="fas fa-unlock-alt"></i> Painel</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> Entrar</a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">Register</a>
                    </li>-->
                @endauth
            </ul>
        @endif
    </div>
</nav>