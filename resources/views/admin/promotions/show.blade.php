<!-- essa extensao abaixo eh o arquivo adminlte.php -->
@extends('adminlte::page')

@section('title', 'Exibir Promoção')

@section('content_header')
    <h1>Exibindo Promoção</h1>
	<a href="{{route('promocoes.index')}}" class="btn btn-default">Voltar</a>
@stop

@section('content')
<div class="box box-warning">
	<div class="box-body">
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<img class="img-responsive thumbnail" src="/images/promotions-images/{{ $promotion->image }}">
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<br><label>Nome do Estabelecimento:</label>
					<p>{{ $promotion->owner_name }}</p>
				</div>
			</div>
		<div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<label>Descrição</label>
					<textarea rows="3" class="form-control" disabled> {!!$promotion->name!!} </textarea>
				</div>
			</div>
		<div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<br><label>Telefone:</label>
					<p>{{ $promotion->phone }}</p>
				</div>
			</div>
		<div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<br><label>Valor atual da promoção:</label>
					<p>{{ 'R$ '.number_format($promotion->price, 2, ',', '.') }}</p>
				</div>
			</div>
		<div>
	</div>
</div>
@stop