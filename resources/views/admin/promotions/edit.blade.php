<!-- essa extensao abaixo eh o arquivo adminlte.php -->
@extends('adminlte::page')

@section('title', 'Editar Promoção')

@section('content_header')
    <h1>Editar Promoção</h1>
	<a href="{{route('promocoes.index')}}" class="btn btn-default">Voltar</a><br><br>
	<div class="alert alert-warning"><b>Atenção! </b>O tamanho máximo para imagens é de 3 MB.</div>
	@include('admin.includes.alerts')
@stop

@section('content')
<form action="{{route('promocoes.update', $promotion->id)}}" method="post" enctype="multipart/form-data">
	<!-- necessario passar esse input pois o metodo update da controller Advertisements soh pode acessador por PUT -->
	<input type="hidden" name="_method" value="put">
	<!-- incluindo formulario -->
	@include('admin.promotions.form')
</form>
@stop

