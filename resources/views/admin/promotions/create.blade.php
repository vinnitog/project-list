<!-- essa extensao abaixo eh o arquivo adminlte.php -->
@extends('adminlte::page')

@section('title', 'Criar Promoção')

@section('content_header')
    <h3>Criar nova promoção</h3>
	<a href="{{route('promocoes.index')}}" class="btn btn-default">Voltar</a>
@stop

@section('content')
<div class="alert alert-warning"><b>Atenção! </b>O tamanho máximo para imagens é de 3 MB.</div>
@include('admin.includes.alerts')
<!-- o enctype serve para nao falhar no post devido ao array de images -->
<form action="{{route('promocoes.store')}}" method="post" enctype="multipart/form-data">
	@include('admin.promotions.form')
</form>
@stop