<!-- essa extensao abaixo eh o arquivo adminlte.php -->
@extends('adminlte::page')

@section('title', 'Promoções')

@section('content_header')
    <h3>Promoções</h3>
    <a href="{{route('promocoes.create')}}" class="btn btn-primary">Criar nova promoção</a><br><br>
    <p>{{$msg}}</p>
@stop

@section('content')
@include('admin.includes.modal')

<div class="box box-info">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Última atualização</th>
                <th>Imagem da Promoção</th>
                <th>Nome do Estabelecimento</th>
                <th>Telefone</th>
                <th>Descrição da Promoção</th>
                <th>Valor da Promoção</th>
                <th>Visualizar</th>
                <th>Editar</th>
                <th>Excluir</th>
                <th>Ativar</th>
                <th>Desativar</th>
            </tr>
            </thead>
            <tbody>
            @foreach($promotions as $promotion)
                <tr>
                    <td>{{date("d/m/Y - H:i:s", strtotime($promotion->updated_at))}}</td>
                    <td><img style="heigth:150px; width:150px;" class="img-responsive col-md-12" src="/images/promotions-images/{{ $promotion->image }}"></td>
                    <td>{{ $promotion->owner_name}}</td>
                    <td>{{ $promotion->phone}}</td>
                    <td>{{ str_limit($promotion->name, $limit = 20, $end = '...') }}</td>
                    <td>{{ 'R$ '.number_format($promotion->price, 2, ',', '.') }}</td>
                    <td>
                        <a href="{{route('promocoes.show', $promotion->id)}}" class="btn btn-default">
                            <span class="fa fa-eye"></span>
                        </a>              
                    </td>
                    <td>
                        <a href="{{route('promocoes.edit', $promotion->id)}}" class="btn btn-warning">
                            <span class="fa fa-edit"></span>
                        </a>   
                    </td>
                    <td data-form="deleteForm">
                        <form action="{{route('promocoes.destroy', $promotion->id)}}" method="post" class="form-delete">
                            {!! csrf_field() !!}
                            <!-- necessario passar esse input pois o metodo update da controller Advertisements soh pode acessador por PUT -->
	                        <input type="hidden" name="_method" value="delete">
                            <button type="submit" value="" class="btn btn-danger"><span class="fa fa-trash"></span></button>
                        </form>
                    </td>
                    <td data-form="publishForm">
                        <form action="{{route('promocoes.publish', $promotion->id)}}" method="post" class="form-publish">
                            {!! csrf_field() !!}
                            <!-- necessario passar esse input pois o metodo update da controller Advertisements soh pode acessador por PUT -->
	                        <input type="hidden" name="_method" value="post">
                            @if($promotion->status == 0 && $promos_status == 0)
                                <button type="submit" value="" class="btn btn-success"><span class="fa fa-check-circle"></span></button>
                            @else
                                <button type="submit" value="" class="btn btn-success hidden"><span class="fa fa-check-circle"></span></button>
                            @endif
                            </label>
                        </form>
                    </td>
                    <td data-form="unpublishForm">
                        <form action="{{route('promocoes.unpublish', $promotion->id)}}" method="post" class="form-unpublish">
                            {!! csrf_field() !!}
                            <!-- necessario passar esse input pois o metodo update da controller Advertisements soh pode acessador por PUT -->
	                        <input type="hidden" name="_method" value="post">
                            @if($promotion->status == 1 && $promos_status > 0)
                                <button type="submit" value="" class="btn btn-warning"><span class="fa fa-times-circle"></span></button>
                            @else
                                <button type="submit" value="" class="btn btn-warning hidden"><span class="fa fa-times-circle"></span></button>
                            @endif
                            </label>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    <!-- paginacao -->
    {!! $promotions->links() !!}
    </div>
</div>
@stop

<!-- script abaixo chama a modal para excluir a promocao -->
@section('js')
<script>
    $('td[data-form="deleteForm"]').on('click', '.form-delete', function(e){
        e.preventDefault();
        var $form = $(this);
        $('#confirm-promotion').modal({ backdrop: 'static', keyboard: false })
            .on('click', '#delete-promotion', function(){
                $form.submit();
            });
    });
</script>

<!--Publicar-->
<script>
    $('td[data-form="publishForm"]').on('click', '.form-publish', function(e){
        e.preventDefault();
        var $form = $(this);
        $('#confirm-publish').modal({ backdrop: 'static', keyboard: false })
            .on('click', '#publish-promotion', function(){
                $form.submit();
            });
    });
</script>

<!--Despublicar-->
<script>
    $('td[data-form="unpublishForm"]').on('click', '.form-unpublish', function(e){
        e.preventDefault();
        var $form = $(this);
        $('#confirm-unpublish').modal({ backdrop: 'static', keyboard: false })
            .on('click', '#unpublish-promotion', function(){
                $form.submit();
            });
    });
</script>
@stop