{!! csrf_field() !!}
<div class="box box-success">
	<div class="box-body">
		<div class="form-group">
			<label for="inputImage">Imagem da Promoção</label><br>
			<input type="file" name="image" id="inputImage" class="">
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label for="inputOwnerName">Nome do Estabelecimento</label>
					<input type="text" class="form-control" name="owner_name" id="inputOwnerName" value="{{ $promotion->owner_name ?? ''}}">
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<label for="inputName">Descrição da Promoção - Tamanho máximo de 200 caracteres <i class="fa fa-3x fa-exclamation"></i></label>
					<textarea class="form-control" name="name" id="inputName" rows="3" maxlength="200">{{ $promotion->name ?? ''}}</textarea>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label for="inputPhone">Telefone</label>
					<input type="text" class="form-control" name="phone" id="inputPhone" value="{{ $promotion->phone ?? ''}}">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="inputPrice">Informe o valor da promoção</label>
			<div class="row">
				<div class="col-md-2">
					<input type="text" class="form-control" name="price" id="inputPrice" value="{{ $promotion->price ?? ''}}">
				</div>
			</div>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Salvar</button>
		</div>
	</div>
</div>

<!-- secao com chamada de jquery externo para criacao de mascaras nos inputs -->
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script>
$(document).ready(function(){
	var price = $("#inputPrice").maskMoney({symbol:'R$ ', showSymbol:true, thousands:',', decimal:'.', symbolStay: true});
	var phone = $("#inputPhone").mask("(99) 99999-9999");
});
</script>
@stop