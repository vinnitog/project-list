<!-- essa extensao abaixo eh o arquivo adminlte.php -->
@extends('adminlte::page')

@section('title', 'Editar Anúncio')

@section('content_header')
    <h1>Editar Anúncio</h1>
	<a href="{{route('anuncios.index')}}" class="btn btn-default">Voltar</a><br><br>
	<div class="alert alert-warning"><b>Atenção! </b>O tamanho máximo para imagens é de 3 MB. Você pode inserir uma ou mais imagens.</div>
	@include('admin.includes.alerts')
@stop

@section('content')
<form action="{{route('anuncios.update', $advertisement->id)}}" method="post" enctype="multipart/form-data">
	<!-- necessario passar esse input pois o metodo update da controller Advertisements soh pode acessador por PUT -->
	<input type="hidden" name="_method" value="put">
	<!-- incluindo formulario -->
	@include('admin.advertisements.edit_form')
</form>
@stop

