<!-- essa extensao abaixo eh o arquivo adminlte.php -->
@extends('adminlte::page')

@section('title', 'Criar Anúncio')

@section('content_header')
    <h3>Criar novo anúncio</h3>
	<a href="{{route('anuncios.index')}}" class="btn btn-default">Voltar</a>
@stop

@section('content')
<div class="alert alert-warning"><b>Atenção! </b>O tamanho máximo para imagens é de 3 MB. Você pode inserir uma ou mais imagens.</div>
@include('admin.includes.alerts')
<!-- o enctype serve para nao falhar no post devido ao array de images -->
<form action="{{route('anuncios.store')}}" method="post" enctype="multipart/form-data">
	@include('admin.advertisements.create_form')
</form>
@stop