<!-- essa extensao abaixo eh o arquivo adminlte.php -->
@extends('adminlte::page')

@section('title', 'Exibir Anúncio')

@section('content_header')
    <h1>Exibindo Anúncio</h1>
	<a href="{{route('anuncios.index')}}" class="btn btn-default">Voltar</a>
@stop

@section('content')
<div class="box box-warning">
	<div class="box-body">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4">
					<label>Imagem Principal:</label>
					<img class="img-responsive thumbnail" src="/images/advertisements-images/{{$advertisement->advertisement_images->first->image['image']}}">
				</div>
				<div class="col-md-12 row">
					@foreach($advertisement->advertisement_images as $key => $images)
						@if($key >0)
							<div class="col-md-2">
								<img class="img-responsive thumbnail" src="/images/advertisements-images/{{$images->image}}">
							</div>
						@endif
					@endforeach
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<br><label>Nome do Estabelecimento:</label>
					<p>{{$advertisement->owner_name}}</p>
				</div>
			</div>
		<div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<label>Descrição</label>
					<textarea rows="10" class="form-control" disabled> {!!$advertisement->description!!} </textarea>
				</div>
			</div>
		<div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<br><label>Email:</label>
					<p>{{$advertisement->email}}</p>
				</div>
			</div>
		<div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<br><label>Telefone:</label>
					<p>{{$advertisement->phone}}</p>
				</div>
			</div>
		<div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<br><label>Rede Social:</label>
					<p>{{$advertisement->social_media}}</p>
				</div>
			</div>
		<div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<label>Categoria:</label>
					<p> {!! $advertisement->category($advertisement->category) !!} </p>
				</div>
			</div>
		<div>
		@if($advertisement->category == 'LAN')
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<br><label>Horario de Funcionamento:</label>
						<p>Abre ás: {{ date('H:i', strtotime($advertisement->open_time)) }} - Fecha ás: {{ date('H:i', strtotime($advertisement->close_time)) }}</p>
					</div>
				</div>
			<div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<br><label>Delivery:</label>
						@if($advertisement->delivery == 1)
							<p>{{ $advertisement->delivery = "SIM"}}</p>
						@else
						<p>{{ $advertisement->delivery = "NÃO"}}</p>
						@endif
					</div>
				</div>
			<div>
			<br><label>Opções variadas:</label><br>
			@foreach($sandwich_variations as $key => $sandwich)
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<input tabindex="1" type="checkbox" name="sandwich[]" id="{{$key}}" value="{{$key}}" style="margin-left:5px;" checked disabled>
							<span>{{$sandwich->sandwich_variation}}</span>
						</div>
					</div>
				<div>
			@endforeach
		@endif
		@if($advertisement->category == 'MEC')
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<br><label>Horario de Funcionamento:</label>
						<p>Abre ás: {{ date('H:i', strtotime($advertisement->open_time)) }} - Fecha ás: {{ date('H:i', strtotime($advertisement->close_time)) }}</p>
					</div>
				</div>
			<div>
			<br><label>Especialidade da Mecânica:</label><br>
			@foreach($mechanic_parts as $key => $mechanic)
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<input tabindex="1" type="checkbox" name="mechanic[]" id="{{$key}}" value="{{$key}}" style="margin-left:5px;" checked disabled>
							<span>{{$mechanic->mechanic_part}}</span>
						</div>
					</div>
				<div>
			@endforeach
		@endif
		@if($advertisement->category == 'CHA')
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<br><label>Quantidade de Quartos:</label>
						<p>{{ $advertisement->room }}</p>
					</div>
				</div>
			<div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<br><label>Acomodações Suportadas:</label>
						<p>{{ $advertisement->capacity }}</p>
					</div>
				</div>
			<div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<br><label>Valor da diária:</label>
						<p>{{ 'R$ '.number_format($advertisement->diary_price, 2, ',', '.') }}</p>
					</div>
				</div>
			<div>
			<br><label>Áreas de Lazer:</label><br>
			@foreach($recreation_areas as $key => $recreation)
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<input tabindex="1" type="checkbox" name="recreation[]" id="{{$key}}" value="{{$key}}" style="margin-left:5px;" checked disabled>
							<span>{{$recreation->recreation_area}}</span>
						</div>
					</div>
				<div>
			@endforeach
			<br><label>Itens Extras:</label><br>
			@foreach($extra_items as $key => $extra)
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<input tabindex="1" type="checkbox" name="extra[]" id="{{$key}}" value="{{$key}}" style="margin-left:5px;" checked disabled>
							<span>{{$extra->extra_item}}</span>
						</div>
					</div>
				<div>
			@endforeach
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<br><label>Alugada para o Feriado:</label>
						@if($advertisement->holiday == 1)
							<p>{{ $advertisement->holiday = "SIM"}}</p>
						@else
						<p>{{ $advertisement->holiday = "NÃO"}}</p>
						@endif
					</div>
				</div>
			<div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<br><label>Alugada para o Natal:</label>
						@if($advertisement->christmas == 1)
							<p>{{ $advertisement->christmas = "SIM"}}</p>
						@else
						<p>{{ $advertisement->christmas = "NÃO"}}</p>
						@endif
					</div>
				</div>
			<div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<br><label>Alugada para o Ano Novo:</label>
						@if($advertisement->new_year == 1)
							<p>{{ $advertisement->new_year = "SIM"}}</p>
						@else
						<p>{{ $advertisement->new_year = "NÃO"}}</p>
						@endif
					</div>
				</div>
			<div>
		@endif
	</div>
</div>
@stop