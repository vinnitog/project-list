{!! csrf_field() !!}
<div class="box box-success">
	<div class="box-body">
		<div class="form-group">
			<label for="inputImage">Imagem do Anúncio</label><br>
			<input type="file" name="image[]" id="inputImage" class="btn btn-default" multiple="true">
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<label for="inputDescription">Descrição - Tamanho máximo de 600 caracteres <i class="fa fa-3x fa-exclamation"></i></label>
					<textarea class="form-control" name="description" id="inputDescription" rows="10" maxlength="600">{{ $advertisement->description ?? ''}}</textarea>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-4">
					<label for="inputEmail">Email para contato</label>
					<input type="text" class="form-control" name="email" id="inputEmail" value="{{ $advertisement->email ?? ''}}">
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label for="inputPhone">Telefone</label>
					<input type="text" class="form-control" name="phone" id="inputPhone" value="{{ $advertisement->phone ?? ''}}">
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label for="inputSocialMedia">Rede Social</label>
					<input type="text" class="form-control" name="social_media" id="inputSocialMedia" value="{{ $advertisement->social_media ?? ''}}">
					<i class="fa fa-2x fa-facebook-square"></i>
					<i class="fa fa-2x fa-instagram"></i>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label for="inputOwnerName">Nome do Estabelecimento</label>
					<input type="text" class="form-control" name="owner_name" id="inputOwnerName" value="{{ $advertisement->owner_name ?? ''}}">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="selectCategory">Categorias:</label>
				<div class="row">
					<div class="col-md-3">
						<select name="category" id="select-category" class="form-control">
							<option value="">-- Selecione uma categoria --</option>
							@foreach ($categories as $key => $category)
								<!-- o if compara de o valor da chave da categoria eh igual a categoria -->
								<option value="{{ $key }}">{{ $category }}</option>
							@endforeach
						</select>
					</div>
				</div>
		</div>

		<!-- ########## CHACARAS ########## -->
		
		<div id="" class="form-group category-div divCategoryCHA" style="display:none">
			<label for="inputRoom">Informe a quantidade de quartos:</label><br>
			<div class="row">
				<div class="col-md-1">
					<input type="text" class="form-control" name="room" id="inputRoom" value="{{ $advertisement->room ?? ''}}">
				</div>
			</div>
		</div>
		<div id="" class="form-group category-div divCategoryCHA" style="display:none">
			<label for="inputCapacity">Informe uma média de acomodações suportadas:</label><br>
			<div class="row">
				<div class="col-md-1">
					<input type="text" class="form-control" name="capacity" id="inputCapacity" value="{{ $advertisement->capacity ?? ''}}">
				</div>
			</div>
		</div>
		<div id="" class="form-group category-div divCategoryCHA" style="display:none">
			<label for="inputDiaryPrice">Informe o valor diário de sua chácara:</label>
			<div class="row">
				<div class="col-md-2">
					<input type="text" class="form-control" name="diary_price" id="inputDiaryPrice" value="{{ $advertisement->diary_price ?? ''}}">
				</div>
			</div>
		</div>
		<div class="form-group category-div divCategoryCHA" style="display:none">
			<label for="selectRecreationArea">Áreas de Lazer:</label>
				<div class="row">
					<div class="col-md-6">
						@foreach ($recreation_areas as $key => $recreation)
							<input tabindex="1" type="checkbox" name="recreation[]" id="{{$recreation}}" value="{{$key}}" style="margin-left:5px;">
							<span>{{$recreation}}</span>
						@endforeach
					</div>
				</div>
		</div>

		<div class="form-group category-div divCategoryCHA" style="display:none">
			<label for="selectExtraItems">Itens Extras:</label>
				<div class="row">
					<div class="col-md-6">
						@foreach ($extra_items as $key => $extra)
							<input tabindex="1" type="checkbox" name="extra[]" id="{{$extra}}" value="{{$key}}" style="margin-left:5px;">
							<span>{{$extra}}</span>
						@endforeach
					</div>
				</div>
		</div>
		<div class="form-group category-div divCategoryCHA" style="display:none">
			<label for="inputHoliday">Marque se a chácara estará alugada no Feriado:</label>
			<div class="row">
				<div class="col-md-2">
					<input tabindex="1" type="checkbox" name="holiday" id="inputHoliday" value="0" style="margin-left:5px;">
				</div>
			</div>
		</div>
		<div class="form-group category-div divCategoryCHA" style="display:none">
			<label for="inputChristmas">Marque se a chácara estará alugada no Natal:</label>
			<div class="row">
				<div class="col-md-2">
					<input tabindex="1" type="checkbox" name="christmas" id="inputChristmas" value="0" style="margin-left:5px;">
				</div>
			</div>
		</div>
		<div class="form-group category-div divCategoryCHA" style="display:none">
			<label for="inputNewYear">Marque se a chácara estará alugada no Ano Novo:</label>
			<div class="row">
				<div class="col-md-2">
					<input tabindex="1" type="checkbox" name="new_year" id="inputNewYear" value="0" style="margin-left:5px;">
				</div>
			</div>
		</div>

		<!-- ########## LANCHONETES ########## -->
		
		<div id="" class="form-group category-div divCategoryLAN divCategoryMEC divCategorySUP" style="display:none">
			<label for="inputHorario">Horario de Funcionamento:</label>
			<div class="row">
				<div class="col-md-2">
					<label for="inputHorario">Abre às:</label>
					<input type="text" class="form-control inputHorario" name="open_time" value="{{ date('H:i', strtotime($advertisement->open_time ?? '00:00')) }}">
				</div>
				<div class="col-md-2">
					<label for="inputHorario">Fecha às:</label>
					<input type="text" class="form-control inputHorario" name="close_time" value="{{ date('H:i', strtotime($advertisement->close_time ?? '00:00')) }}">
				</div>
			</div>
		</div>
		<div id="" class="form-group category-div divCategoryLAN divCategorySUP" style="display:none">
			<label for="inputDelivery">Informe se você realiza entregas (delivery):</label>
			<div class="row">
				<div class="col-md-2">
					<input tabindex="1" type="checkbox" name="delivery" id="inputDelivery" value="0" style="margin-left:5px;">
				</div>
			</div>
		</div>
		<div class="form-group category-div divCategoryLAN" style="display:none">
			<label for="selectSandwichVariation">Informe se você possui algumas dessas variações em seu cardápio:</label>
				<div class="row">
					<div class="col-md-6">
						@foreach ($sandwich_variations as $key => $sandwich)
							<input tabindex="1" type="checkbox" name="sandwich[]" id="{{$key}}" value="{{$key}}" style="margin-left:5px;">
							<span>{{$sandwich}}</span>
						@endforeach
					</div>
				</div>
		</div>
		<div id="" class="form-group category-div" style="display:none">
			<div class="row">
				<div class="col-md-3">
					<label for="inputPrice">Informe o valor produto</label>
					<input type="text" class="form-control" name="price" id="inputPrice" value="{{ $advertisement->price ?? ''}}">
				</div>
			</div>
		</div>

		<!-- ########## MECÂNICAS ########## -->

		<div class="form-group category-div divCategoryMEC" style="display:none">
			<label for="selectMechanicPart">Qual a especialidade de sua mecânica:</label>
				<div class="row">
					<div class="col-md-6">
						@foreach ($mechanic_parts as $key => $mechanic)
							<input tabindex="1" type="checkbox" name="mechanic[]" id="{{$key}}" value="{{$key}}" style="margin-left:5px;">
							<span>{{$mechanic}}</span>
						@endforeach
					</div>
				</div>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success" style="margin-top:25px;">Salvar</button>
		</div>
	</div>
</div>

<!-- secao com chamada de jquery externo para criacao de mascaras nos inputs -->
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script>
	$(document).ready(function(){
		var time = $(".inputHorario").mask("00:00");
		var phone = $("#inputPhone").mask("(99) 99999-9999");
		var price = $("#inputPrice").maskMoney({symbol:'R$ ', showSymbol:true, thousands:',', decimal:'.', symbolStay: true});
		var diary_price = $("#inputDiaryPrice").maskMoney({symbol:'R$ ', showSymbol:true, thousands:',', decimal:'.', symbolStay: true});
		//var average_price = $("#inputAveragePrice").maskMoney({symbol:'R$ ', showSymbol:true, thousands:',', decimal:'.', symbolStay: true});
	});
</script>
<script>
	$(document).ready(function(){
		// run on change for the selectbox
		$( "#select-category" ).change(function() {  
                updateCategoryDivs();
        });
            
		// handle the updating of the duration divs
		function updateCategoryDivs() {
			// hide all form-duration-divs
			$('.category-div').hide();
				
			var divKey = $( "#select-category option:selected" ).val();                
			$('.divCategory'+divKey).show();
		}        
	
		// run at load, for the currently selected div to show up
		updateCategoryDivs();
	});
</script>
@stop