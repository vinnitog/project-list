<!-- essa extensao abaixo eh o arquivo adminlte.php -->
@extends('adminlte::page')

@section('title', 'Anúncios')

@section('content_header')
    <h3>Anúncios</h3>
    @if($total_advertisements_per_user >= 1)
        <a class="btn btn-primary" disabled>Criar novo anúncio</a><br><br>
        <p><b>Você atingiu o número máximo de anúncios.</b></p>
    @else
        <a href="{{route('anuncios.create')}}" class="btn btn-primary">Criar novo anúncio</a><br><br>
    @endif
    <p>{{$msg}}</p>
@stop

@section('content')
@include('admin.includes.modal')

<form action="{{ route('anuncios.searchAdminAdvertisements') }}" method="POST" class="form form-inline">
    {!! csrf_field() !!}
    <input type="hidden" name="_method" value="post">
    <input type="text" name="owner_name" class="form-control" placeholder="Nome do Estabelecimento">
    <input type="text" name="descricao" class="form-control" placeholder="Descrição">
    <select name="category" id="" class="form-control">
        <option value="">Todas as categorias</option>
        @foreach ($categories as $key => $category)
            <option value="{{ $key }}">{{ $category }}</option>
        @endforeach
    </select>
    <button type="submit" class="btn btn-info"><i class="fa fa-search"></i></button>
</form><br>

<div class="box box-info">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Última atualização</th>
                <th>Imagem do Anúncio</th>
                <th>Categoria do Anúncio</th>
                <th>Nome do Estabelecimento</th>
                <th>Descrição do Anúncio</th>
                <th>Email</th>
                <th>Telefone</th>
                <th>Rede Social</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($advertisements as $advertisement)
                <tr>
                    <td>{{date("d/m/Y - H:i:s", strtotime($advertisement->updated_at))}}</td>
                    <td><img style="heigth:150px; width:150px;" class="img-responsive" src="/images/advertisements-images/{{$advertisement->advertisement_images->first->image['image']}}"></td>
                    <td>{{ $advertisement->category($advertisement->category) }}</td>
                    <td>{{ $advertisement->owner_name}}</td>
                    <!-- a funcao abaixo faz um truncate da string, apresentando apenas os 50 primeiros caracteres -->
                    <td>{{ str_limit($advertisement->description, $limit = 20, $end = '...') }}</td>
                    <td>{{ $advertisement->email }}</td>
                    <td>{{ $advertisement->phone }}</td>
                    <td>{{ $advertisement->social_media }}</td>
                    <td>
                        <a href="{{route('anuncios.show', $advertisement->id)}}" class="btn btn-default">
                            <span class="fa fa-eye"></span>
                        </a>              
                    </td>
                    <td>
                        <a href="{{route('anuncios.edit', $advertisement->id)}}" class="btn btn-warning">
                            <span class="fa fa-edit"></span>
                        </a>   
                    </td>
                    <td data-form="deleteForm">
                        <form action="{{route('anuncios.destroy', $advertisement->id)}}" method="post" class="form-delete">
                            {!! csrf_field() !!}
                            <!-- necessario passar esse input pois o metodo update da controller Advertisements soh pode acessador por PUT -->
	                        <input type="hidden" name="_method" value="delete">
                            <button type="submit" value="" class="btn btn-danger"><span class="fa fa-trash"></span></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    <!-- paginacao -->
    @if (isset($dataForm))
        <!-- faz com que os parametros do filtro sejam mantidos ate quando paginar -->    
        {!! $advertisements->appends($dataForm)->links() !!}
    @else
        {!! $advertisements->links() !!}
    @endif
    </div>
</div>
@stop

<!-- script abaixo chama a modal para excluir o anuncio -->
@section('js')
    <script>
        $('td[data-form="deleteForm"]').on('click', '.form-delete', function(e){
            e.preventDefault();
            var $form = $(this);
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete-advertisement', function(){
                    $form.submit();
                });
        });
    </script>
@stop