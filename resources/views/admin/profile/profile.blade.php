<!-- essa extensao abaixo eh o arquivo adminlte.php -->
@extends('adminlte::page')

@section('title', 'Perfil')

@section('content_header')
    <h3>Editar Perfil</h3>
@stop

@section('content')
<div class="alert alert-warning"><b>Atenção! </b>O tamanho máximo para imagens é de 3 MB.</div>
@include('admin.includes.alerts')
<form action="{{ route('profileUpdate') }}" method="POST" enctype="multipart/form-data">
{!! csrf_field() !!}
    <div class="box box-success">
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <label for="name">Nome</label><br>
                        <input type="text" name="name" id="userName" class="form-control" value="{{ auth()->user()->name }}" placeholder="Nome">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <label for="email">Email</label><br>
                        <input type="email" name="email" id="userEmail" class="form-control" value="{{ auth()->user()->email }}" placeholder="Email">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <label for="password">Senha</label><br>
                        <input type="password" name="password" id="userPassword" class="form-control" placeholder="Senha">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        @if(auth()->user()->image != null)
                            <img src="/images/users-images/{{ auth()->user()->image }}" style="width:100px; height:100px; border-radius:50%;">
                            <!--<img src="{{ url('storage/users'.auth()->user()->image) }}" style="width:100px; height:100px; border-radius:50%;">-->
                        @else
                            <img src="/images/users-images/noimage.png" style="width:100px; height:100px;">
                        @endif
                    </div>
                </div>
            </div>
            <div class"form-group">
                <div class="row">
                    <div class="col-md-2">
                        <label for="image">Imagem</label>
                        <input type="file" class="" name="image">
                    </div>
                </div>
            </div><br>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Salvar</button>
            </div>
        </div>
    </div>
</form>
@stop

@section('js')
@stop