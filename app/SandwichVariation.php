<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SandwichVariation extends Model
{
    protected $fillable = [
        'advertisement_id', 'sandwich_variation'
    ];

    public function advertisements(){
        return $this->belongsTo(Advertisement::class);
    }
}
