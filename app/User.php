<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Advertisement;
use App\Promotion;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //user possui varios advertisements
    public function advertisements(){
        return $this->hasMany(Advertisement::class);
    }

    //user possui varias promotions
    public function promotions(){
        return $this->hasMany(Promotion::class);
    }
}
