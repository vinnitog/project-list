<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementImage extends Model
{
    protected $table = "advertisement_images";
    
    protected $fillable = [
        'advertisement_id', 'image'
    ];

    public function advertisements(){
        return $this->belongsTo(Advertisement::class);
    }
}
