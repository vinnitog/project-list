<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // renomeando as acoes para pt-br
        Route::resourceVerbs([
            'create' => 'criar',
            'edit' => 'editar',
            'show' => 'exibir',
            'destroy' => 'deletar',
        ]);

       // forcando url's para https para deploy heroku
       //URL::forceScheme('https');
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
