<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PromotionUpdateFormRequest;
use App\Http\Requests\PromotionValidationFormRequest;
use Intervention\Image\Facades\Image;
use App\Promotion;
use App\User;
use File;

class PromotionsController extends Controller
{
    private $totalPages = 5;
    
    public function index(Promotion $promotions){

        $promotions = auth()->user()->promotions()->paginate($this->totalPages);
        //$promos_status = Promotion::all()->where('status', 1)->count();
        $promos_status = auth()->user()->promotions()->where('status', 1)->count();

        // tratamento para se nao houverem anuncios ainda criados
        $msg = '';
        if($promotions->isEmpty()){
            $msg = 'Não há promoções criadas';
        }
        
        return view('admin.promotions.index', compact('promotions', 'msg', 'promos_status'));
    }

    public function create(){

        return view('admin.promotions.create');
    }

    public function update(PromotionUpdateFormRequest $request, $id){

        $promotion = Promotion::findOrFail($id);

        // validando imagem e gerando novo nome
        $promo_img = $promotion->image;
        $image_name = $request->image;
        
        if($request->hasFile('image') && $request->file('image')->isValid()){
            
            //deleta a imagem da pasta public
            File::delete('images/promotions-images/'.$promo_img);
            
            $image = $request->file('image');
            $image_name = $promotion->id.$image->getClientOriginalName();
            Image::make($image)->save( public_path('/images/promotions-images/'.$image_name));
            //$request->image->storeAs('users', $image_name);
            $request->image = $image_name;
            //$request->image->save();
        }

        if($image_name == null){
            $image_name = $promotion->image;
        }

        // formata campo price para que nao de erro no banco de dados, removendo as virgulas
        $price = $request->price;
        $price_formated = floatval(preg_replace('/[^\d.]/', '', $price));

        $values = [
            'user_id' => auth()->id(), 
            'image' => $image_name,
            'price' => $price_formated,
            'name' => $request->name,
            'phone' => $request->phone,
            'owner_name' => $request->owner_name
        ];

        //atualiza requisicao
        $promotion->update($values);
        
    	//redireciona para a pagina de exibicao passando o id do anuncio em questao
    	return response()->redirectToRoute('promocoes.show', $id);
    }

    public function show($id){

        $promotion = Promotion::findOrFail($id);
        return view('admin.promotions.show', compact('promotion'));
    }

    public function edit(Promotion $promotion, $id){

        $promotion = Promotion::findOrFail($id);
        return view('admin.promotions.edit', compact('promotion'));
    }
    
    public function store(PromotionValidationFormRequest $request){
        
        // formata campo price para que nao de erro no banco de dados, removendo as virgulas
        $price = $request->price;
        $price_formated = floatval(preg_replace('/[^\d.]/', '', $price));

        // validando imagem e gerando novo nome
        $image_name = $request->image;
        if($request->hasFile('image') && $request->file('image')->isValid()){

            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            Image::make($image)->save( public_path('/images/promotions-images/'.$image_name));
            //$request->image->storeAs('users', $image_name);
            $request->image = $image_name;
            //$request->image->save();
        }

        if($image_name == null){
            $image_name = $promotion->image;
        }

        Promotion::create([
            'user_id' => auth()->id(), 
            'image' => $image_name,
            'price' => $price_formated,
            'name' => $request->name,
            'phone' => $request->phone,
            'owner_name' => $request->owner_name
        ]);

    	return response()->redirectToRoute('promocoes.index');
    }

    public function publish($id){

        $promotion = Promotion::findOrFail($id);
        $status = $promotion->status;
        if($status == 0){
            $status = 1;
        }
        $promotion->update(['status' => $status]);
        return response()->redirectToRoute('promocoes.index');
    }

    public function unpublish($id){

        $promotion = Promotion::findOrFail($id);
        $status = $promotion->status;
        if($status == 1){
            $status = 0;
        }
        $promotion->update(['status' => $status]);
        return response()->redirectToRoute('promocoes.index');
    }

    public function destroy($id){

        $promotion = Promotion::findOrFail($id);
        $promotion->delete();
        return response()->redirectToRoute('promocoes.index');
    }
}
