<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Mail;

class ContactsController extends Controller
{
    public function contact(){
        return view('site.contact.contact');
    }

    public function store(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
            ]);
     
        Contact::create($request->all());
        
        $email = $request->get('email');
        Mail::send('site.contact.mailme',
        array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'bodyMessage' => $request->get('message')
        ), function($message) use($email){
            $message->from($email);
            $message->to('atendimentogearlist@gmail.com', 'Admin')->subject('Contato Gearlist');
        });

        return back()->with('success', 'Agradecemos seu contato, retornaremos em breve!');
    }
}
