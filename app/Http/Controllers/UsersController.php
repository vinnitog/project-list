<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserUpdateFormRequest;
use Intervention\Image\Facades\Image;
use File;

class UsersController extends Controller
{
    public function profile(){

        return view ('admin.profile.profile');
    }

    public function profileUpdate(UserUpdateFormRequest $request){
        
        // caso o usuario defina uma nova senha ira criptografa-la antes de enviar pro banco de dados
        if($request['password'] !=null){
            $request['password'] = bcrypt($request['password']);
        }else{
            // para evitar erro de integridade, deleta o password caso venha nulo
            $request['password'] = auth()->user()->password;
        }

        // validando imagem e gerando novo nome
        $user_img = auth()->user()->image;
        $image_name = $request->image;

        if($request->hasFile('image') && $request->file('image')->isValid()){

            //deleta a imagem da pasta public
            File::delete('images/users-images/'.$user_img);

            $image = $request->file('image');
            $image_name = auth()->user()->id.$image->getClientOriginalName();
            Image::make($image)->save( public_path('/images/users-images/'.$image_name));
            //$request->image->storeAs('users', $image_name);
            auth()->user()->image = $image_name;
            auth()->user()->save();
        }

        if($image_name == null){
            $image_name = auth()->user()->image;
        }
        
        $update = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'image' => $image_name,
        ];

        $update_user = auth()->user()->update($update);

        if($update_user){
            return redirect()->route('profile')->with('success', 'Sucesso ao atualizar usuário!');
        }else{
            return redirect()->back()->with('error', 'Falha ao atualizar usuário!');
        }
    }
}
