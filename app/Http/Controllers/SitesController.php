<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advertisement;
use App\Promotion;

class SitesController extends Controller
{
    private $totalPages = 12;
    
    public function index(Advertisement $advertisement){
        
        $advertisements = Advertisement::paginate($this->totalPages);
        $categories = $advertisement->category();
        return view('site.home.index', compact('advertisements', 'categories'));
    }

    // metodo responsavel pela filtro de anuncios
    public function searchPublicAdvertisements(Request $request, Advertisement $advertisement){

        // pega todos os campos exceto o campo token, senao poderia ser $request->all(); normal, dessa forma nao fica o token na url
        $dataForm = $request->except('_token', '_method');

        //pegando resultado da consulta feita pelo metodo na Model de Advertisements
        $advertisements = $advertisement->searchAdvertisements($dataForm, $this->totalPages);
        
        // tratamento para se nao houverem registros encontrados
        $msg = '';
        if($advertisements->isEmpty()){
            $msg = 'Não foram encontrados anúncios com este nome.';
        }

        $categories = $advertisement->category();
        return view('site.home.buscar', compact('advertisements', 'msg', 'dataForm', 'categories'));
    }

    public function promotion(){

        $promotions = Promotion::where('status', 1)->paginate($this->totalPages);
        return view('site.promotion.promotion', compact('promotions'));
    }

    public function about(){
        return view('site.about.about');
    }

    public function sign(){
        return view('site.sign.sign');
    }
}
