<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advertisement;
use App\User;
use App\AdvertisementImage;
use App\RecreationArea;
use App\ExtraItem;
use App\SandwichVariation;
use App\MechanicPart;
use App\Http\Requests\AdvertisementValidationFormRequest;
use App\Http\Requests\AdvertisementUpdateFormRequest;
use Intervention\Image\Facades\Image;
use File;

class AdvertisementsController extends Controller
{
    private $totalPages = 5;

    public function index(Advertisement $advertisement){

        $advertisements = auth()->user()->advertisements()->paginate($this->totalPages);
        $total_advertisements_per_user = auth()->user()->advertisements()->count();

        // tratamento para se nao houverem anuncios ainda criados
        $msg = '';
        if($advertisements->isEmpty()){
            $msg = 'Não há anúncios criados';
        }

        //categorias
        $categories = $advertisement->category();

    	//transformando variavel advertisements e msg em um array
    	return view('admin.advertisements.index', compact('advertisements', 'msg', 'categories', 'total_advertisements_per_user'));
    }

    public function create(Advertisement $advertisement)
    {
        $categories = $advertisement->category();
        $recreation_areas = $advertisement->recreation_area();
        $extra_items = $advertisement->extra_item();
        $sandwich_variations = $advertisement->sandwich_variation();
        $mechanic_parts = $advertisement->mechanic_part();

        return view('admin.advertisements.create', compact('advertisement', 'categories', 'recreation_areas', 'extra_items', 'sandwich_variations', 'mechanic_parts'));
    }

    public function update(AdvertisementUpdateFormRequest $request, $id){

    	// encontra o usuario ou falha
        $advertisement = Advertisement::findOrFail($id);

        // resgatando categoria
        $category = $request->category;
        if($category == null){
            $category = $advertisement->category;
        }

        // formata campo diary_price para que nao de erro no banco de dados, removendo as virgulas
        $diary_price = $request->diary_price;
        $diary_price_formated = floatval(preg_replace('/[^\d.]/', '', $diary_price));

        // formata campo data para que nao de erro no banco de dados, inserindo uma data ficticia
        $open_time = '2018-01-01 '.$request->open_time;
        $close_time = '2018-01-01 '.$request->close_time;

        //resgatando area de lazer existente desse anuncio
        $existent_rec = Advertisement::find($id)->recreation_areas()->get(['recreation_area']);
        $ext_rec = [];

        //resgatando items extras da area de lazer existente desse anuncio
        $existent_extra = Advertisement::find($id)->extra_items()->get(['extra_item']);
        $ext_extra = [];

        //resgatando variacao de sandwich existente desse anuncio
        $existent_sand = Advertisement::find($id)->sandwich_variations()->get(['sandwich_variation']);
        $ext_sand = [];

        //resgatando variacao de mecanica existente desse anuncio
        $existent_mec = Advertisement::find($id)->mechanic_parts()->get(['mechanic_part']);
        $ext_mec = [];

        //resgatando imagem existente desse anuncio
        $existent_img = Advertisement::find($id)->advertisement_images()->get(['image']);
        $ext_img = [];

        if($category == 'CHA'){

            $holiday = "";
            $christmas = "";
            $new_year = "";

            if($request->holiday == null){
                $holiday = 0;
            }else{
                $holiday = 1;
            }
            if($request->christmas == null){
                $christmas = 0;
            }else{
                $christmas = 1;
            }
            if($request->new_year == null){
                $new_year = 0;
            }else{
                $new_year = 1;
            }

            $cha_values = [
                'user_id' => auth()->id(), 
                'description' => $request->description,
                'email' => $request->email,
                'phone' => $request->phone,
                'social_media' => $request->social_media,
                'owner_name' => $request->owner_name,
                'category' => $category,
                'room' => $request->room,
                'capacity' => $request->capacity,
                'diary_price' => $diary_price_formated,
                'holiday' => $holiday,
                'christmas' => $christmas,
                'new_year' => $new_year,
            ];

            //atualiza requisicao
            $advertisement->update($cha_values);
            
            foreach ($existent_rec as $rec) {
                $ext_rec = $rec->recreation;
            }

            if($request->recreation == null){
                $request->recreation = $ext_rec;
                //salvando checkbox's das chacaras
                self::updateAdvtRecreation($request, $advertisement);
                
            }elseif($ext_rec != $request->recreation){
                $advertisement->recreation_areas()->delete([
                    'advertisement_id' => $advertisement->id
                    //'recreation_area' => $request->recreation
                ]);
                //salvando checkbox's das chacaras
                self::updateAdvtRecreation($request, $advertisement);
            }

            foreach ($existent_extra as $extra_item) {
                $ext_extra = $extra_item->extra;
            }

            if($request->extra == null){
                $request->extra = $ext_extra;
                //salvando checkbox's dos items extras das chacaras
                self::updateAdvtExtraItems($request, $advertisement);
                
            }elseif($ext_extra != $request->extra){
                $advertisement->extra_items()->delete([
                    'advertisement_id' => $advertisement->id
                    //'item' => $request->item
                ]);
                //salvando checkbox's dos items extras das chacaras
                self::updateAdvtExtraItems($request, $advertisement);
            }
                  
        }elseif($category == 'LAN'){

            $delivery = "";
            if($request->delivery == null){
                $delivery = 0;
            }else{
                $delivery = 1;
            }

            $lan_values = [
                'user_id' => auth()->id(), 
                'description' => $request->description,
                'open_time' => $open_time,
                'close_time' => $close_time,
                'email' => $request->email,
                'phone' => $request->phone,
                'social_media' => $request->social_media,
                'owner_name' => $request->owner_name,
                'category' => $request->category,
                'delivery' => $delivery,
            ];

            //atualiza requisicao
            $advertisement->update($lan_values);

            foreach ($existent_sand as $sand) {
                $ext_sand = $sand->sandwich;
            }

            if($request->sandwich == null){
                $request->sandwich = $ext_sand;
                //salvando checkbox's das lanchonetes
                self::updateAdvtSandwich($request, $advertisement);

            }elseif($ext_sand != $request->sandwich){
                $advertisement->sandwich_variations()->delete([
                    'advertisement_id' => $advertisement->id
                    //'sandwich_variation' => $request->sandwich
                ]);
                //salvando checkbox's das lanchonetes
                self::updateAdvtSandwich($request, $advertisement);
            }
        }elseif($category == 'MEC'){

            $mec_values = [
                'user_id' => auth()->id(), 
                'description' => $request->description,
                'open_time' => $open_time,
                'close_time' => $close_time,
                'email' => $request->email,
                'phone' => $request->phone,
                'social_media' => $request->social_media,
                'owner_name' => $request->owner_name,
                'category' => $category,
            ];

            //atualiza requisicao
            $advertisement->update($mec_values);

            foreach ($existent_mec as $mec) {
                $ext_mec = $mec->mechanic;
            }

            if($request->mechanic == null){
                $request->mechanic = $ext_mec;
                //salvando checkbox's das mecanicas
                self::updateAdvtMechanic($request, $advertisement);

            }elseif($ext_mec != $request->mechanic){
                $advertisement->mechanic_parts()->delete([
                    'advertisement_id' => $advertisement->id
                    //'mechanic_part' => $request->mechanic
                ]);
                //salvando checkbox's das lanchonetes
                self::updateAdvtMechanic($request, $advertisement);
            }            

        }elseif($category == 'SUP'){
            
            $delivery = "";
            if($request->delivery == null){
                $delivery = 0;
            }else{
                $delivery = 1;
            }

            $sup_values = [
                'user_id' => auth()->id(), 
                'description' => $request->description,
                'open_time' => $open_time,
                'close_time' => $close_time,
                'email' => $request->email,
                'phone' => $request->phone,
                'social_media' => $request->social_media,
                'owner_name' => $request->owner_name,
                'category' => $request->category,
                'delivery' => $delivery,
            ];

            //atualiza requisicao
            $advertisement->update($sup_values);
        }

        foreach ($existent_img as $img) {
            $ext_img = $img->image;
        }

        if($request->image == null){
            $request->image = $ext_img;
            //salvando imagem/imagens do anuncio
            self::updateAdvtImage($request, $advertisement);

        }elseif($ext_img != $request->image){

            //deleta a imagem da pasta public
            File::delete('images/advertisements-images/'.$ext_img);
            $advertisement->advertisement_images()->delete([
                'advertisement_id' => $advertisement->id
                //'image' => $request->image
            ]);

            self::updateAdvtImage($request, $advertisement);
        }

    	//redireciona para a pagina de exibicao passando o id do anuncio em questao
    	return response()->redirectToRoute('anuncios.show', $id);
    }

    public function show($id){

        $advertisement = Advertisement::findOrFail($id);
        $recreation_areas = Advertisement::find($id)->recreation_areas()->get(['recreation_area']);
        $extra_items = Advertisement::find($id)->extra_items()->get(['extra_item']);
        $sandwich_variations = Advertisement::find($id)->sandwich_variations()->get(['sandwich_variation']);
        $mechanic_parts = Advertisement::find($id)->mechanic_parts()->get(['mechanic_part']);

    	return view('admin.advertisements.show', compact('advertisement', 'recreation_areas', 'extra_items', 'sandwich_variations', 'mechanic_parts'));
    }

    public function edit($id){
        
        $advertisement = Advertisement::findOrFail($id);
        $categories = $advertisement->category();
        $recreation_areas = $advertisement->recreation_area();
        $extra_items = $advertisement->extra_item();
        $sandwich_variations = $advertisement->sandwich_variation();
        $mechanic_parts = $advertisement->mechanic_part();

    	return view('admin.advertisements.edit', compact('advertisement', 'categories', 'recreation_areas', 'extra_items', 'sandwich_variations', 'mechanic_parts'));
    }

    // metodo que efetivamente cria um novo registro de anuncio
    public function store(AdvertisementValidationFormRequest $request, Advertisement $advertisement){

        $total_advertisements_per_user = auth()->user()->advertisements()->count();
        
        if($total_advertisements_per_user < 1){

            // formata campo diary_price para que nao de erro no banco de dados, removendo as virgulas
            $diary_price = $request->diary_price;
            $diary_price_formated = floatval(preg_replace('/[^\d.]/', '', $diary_price));

            // formata campo data para que nao de erro no banco de dados, inserindo uma data ficticia
            $open_time = '9999-12-12 '.$request->open_time;
            $close_time = '9999-12-12 '.$request->close_time;

            if($request->category == 'CHA')
            {   
                if($request->holiday == null){
                    $holiday = 0;
                }else{
                    $holiday = 1;
                }
                if($request->christmas == null){
                    $christmas = 0;
                }else{
                    $christmas = 1;
                }
                if($request->new_year == null){
                    $new_year = 0;
                }else{
                    $new_year = 1;
                }
                

                $cha_values = [
                    'user_id' => auth()->id(), 
                    'description' => $request->description,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'social_media' => $request->social_media,
                    'owner_name' => $request->owner_name,
                    'category' => $request->category,
                    'room' => $request->room,
                    'capacity' => $request->capacity,
                    'diary_price' => $diary_price_formated,
                    'holiday' => $holiday,
                    'christmas' => $christmas,
                    'new_year' => $new_year,
                ];

                //criando anuncio
                $advertisement = Advertisement::create($cha_values);
                //salvando checkbox's das chacaras
                self::createAdvtRecreation($request, $advertisement);
                //salvando checkbox's dos items extras das chacaras
                self::createAdvtExtraItems($request, $advertisement);

            }elseif($request->category == 'LAN'){

                if($request->delivery == null){
                    $delivery = 0;
                }else{
                    $delivery = 1;
                }
                
                $lan_values = [
                    'user_id' => auth()->id(), 
                    'description' => $request->description,
                    'open_time' => $open_time,
                    'close_time' => $close_time,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'social_media' => $request->social_media,
                    'owner_name' => $request->owner_name,
                    'category' => $request->category,
                    'delivery' => $delivery,
                ];

                //dd($lan_values);

                //criando anuncio
                $advertisement = Advertisement::create($lan_values);        
                //salvando checkbox's das lanchonetes
                self::createAdvtSandwich($request, $advertisement);

            }elseif($request->category == 'MEC'){
                
                $mec_values = [
                    'user_id' => auth()->id(), 
                    'description' => $request->description,
                    'open_time' => $open_time,
                    'close_time' => $close_time,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'social_media' => $request->social_media,
                    'owner_name' => $request->owner_name,
                    'category' => $request->category,
                ];

                //dd($mec_values);

                //criando anuncio
                $advertisement = Advertisement::create($mec_values);        
                //salvando checkbox's das mecanicas
                self::createAdvtMechanic($request, $advertisement);

            }elseif($request->category == 'SUP'){

                if($request->delivery == null){
                    $delivery = 0;
                }else{
                    $delivery = 1;
                }

                $sup_values = [
                    'user_id' => auth()->id(), 
                    'description' => $request->description,
                    'open_time' => $open_time,
                    'close_time' => $close_time,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'social_media' => $request->social_media,
                    'owner_name' => $request->owner_name,
                    'category' => $request->category,
                    'delivery' => $delivery,
                ];
    
                //criando anuncio
                $advertisement = Advertisement::create($sup_values); 
            }

            //salvando imagem/imagens do anuncio
            self::createAdvtImage($request, $advertisement);

            return response()->redirectToRoute('anuncios.index');
        }else{
            return redirect()->back()->with('error', 'Falha ao criar Anúncio! Você atingiu o número máximo de anúncios.');
        }
        
    }

    // ##### CRIACAO ##### //

    //cria imagens dos anuncios
    public function createAdvtImage(AdvertisementValidationFormRequest $request, Advertisement $advertisement){
        if($request->hasFile('image')){
            foreach ($request->file('image') as $image) {
                //concatena o nome com hora, minuto e segundo
                $name = $advertisement->id.$image->getClientOriginalName();
                Image::make($image)->save( public_path('/images/advertisements-images/'.$name));
                
                //criando imagens para o anuncio em questao
                AdvertisementImage::create([
                    'advertisement_id' => $advertisement->id,
                    'image' => $name
                ]);
            }
        }
    }

    //cria areas de lazer 
    public function createAdvtRecreation(AdvertisementValidationFormRequest $request, Advertisement $advertisement){
        if($request->recreation){
            foreach ($request->recreation as $recreation) {
                $rec = $recreation;
                
                RecreationArea::create([
                    'advertisement_id' => $advertisement->id,
                    'recreation_area' => $rec
                ]);
            }
        }
    }

    //cria items extras da areas de lazer 
    public function createAdvtExtraItems(AdvertisementValidationFormRequest $request, Advertisement $advertisement){
        if($request->extra){
            foreach ($request->extra as $extra_item) {
                $extra = $extra_item;
                
                ExtraItem::create([
                    'advertisement_id' => $advertisement->id,
                    'extra_item' => $extra
                ]);
            }
        }
    }

    //cria variacao de sandwiches
    public function createAdvtSandwich(AdvertisementValidationFormRequest $request, Advertisement $advertisement){
        if($request->sandwich){
            foreach ($request->sandwich as $sandwich) {
                $sand = $sandwich;
                
                SandwichVariation::create([
                    'advertisement_id' => $advertisement->id,
                    'sandwich_variation' => $sand
                ]);
            }
        }
    }

    //cria variacao de mecanicas
    public function createAdvtMechanic(AdvertisementValidationFormRequest $request, Advertisement $advertisement){
        if($request->mechanic){
            foreach ($request->mechanic as $mechanic) {
                $mec = $mechanic;
                
                MechanicPart::create([
                    'advertisement_id' => $advertisement->id,
                    'mechanic_part' => $mec
                ]);
            }
        }
    }

    // ##### ATUALIZACAO ##### //

    //atualiza imagens dos anuncios
    public function updateAdvtImage(AdvertisementUpdateFormRequest $request, Advertisement $advertisement){
        if($request->hasFile('image')){
            foreach ($request->file('image') as $image) {
                //concatena o nome com hora, minuto e segundo
                $name = $advertisement->id.$image->getClientOriginalName();
                Image::make($image)->save( public_path('/images/advertisements-images/'.$name));
                
                //criando imagens para o anuncio em questao
                AdvertisementImage::create([
                    'advertisement_id' => $advertisement->id,
                    'image' => $name
                ]);
            }
        }
    }

    //atualiza areas de lazer 
    public function updateAdvtRecreation(AdvertisementUpdateFormRequest $request, Advertisement $advertisement){
        if($request->recreation){
            foreach ($request->recreation as $recreation) {
                $rec = $recreation;

                RecreationArea::create([
                    'advertisement_id' => $advertisement->id,
                    'recreation_area' => $rec
                ]);
            }
        }
    }

    //atualiza items extras da areas de lazer 
    public function updateAdvtExtraItems(AdvertisementUpdateFormRequest $request, Advertisement $advertisement){
        if($request->extra){
            foreach ($request->extra as $extra_item) {
                $extra = $extra_item;
                
                ExtraItem::create([
                    'advertisement_id' => $advertisement->id,
                    'extra_item' => $extra
                ]);
            }
        }
    }

    //atualiza variacao de sandwiches
    public function updateAdvtSandwich(AdvertisementUpdateFormRequest $request, Advertisement $advertisement){
        if($request->sandwich){
            foreach ($request->sandwich as $sandwich) {
                $sand = $sandwich;
                
                SandwichVariation::create([
                    'advertisement_id' => $advertisement->id,
                    'sandwich_variation' => $sand
                ]);
            }
        }
    }

    //atualiza variacao de mecanicas
    public function updateAdvtMechanic(AdvertisementUpdateFormRequest $request, Advertisement $advertisement){
        if($request->mechanic){
            foreach ($request->mechanic as $mechanic) {
                $mec = $mechanic;
                
                MechanicPart::create([
                    'advertisement_id' => $advertisement->id,
                    'mechanic_part' => $mec
                ]);
            }
        }
    }

    public function destroy($id){
        $advertisement = Advertisement::findOrFail($id);
        $advertisement->delete();
        return response()->redirectToRoute('anuncios.index');
    }

    // metodo responsavel pela filtro de anuncios
    public function searchAdminAdvertisements(Request $request, Advertisement $advertisement){

        // pega todos os campos exceto o campo token, senao poderia ser $request->all(); normal, dessa forma nao fica o token na url
        $dataForm = $request->except('_token', '_method');

        //pegando resultado da consulta feita pelo metodo na Model de Advertisements
        $advertisements = $advertisement->searchUserAdvertisements($dataForm, $this->totalPages);
        
        // tratamento para se nao houverem registros encontrados
        $msg = '';
        if($advertisements->isEmpty()){
            $msg = 'Registro não encontrado';
        }

        $categories = $advertisement->category();
        $total_advertisements_per_user = auth()->user()->advertisements()->count();
        return view('admin.advertisements.index', compact('advertisements', 'msg', 'dataForm', 'categories', 'total_advertisements_per_user'));
    }
}
