<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdvertisementValidationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required',
            'image.*' => 'mimes:jpg,jpeg,png,svg|max:5000',
            'description' => 'required|max:600',
            'email' => 'max:255',
            'phone' => 'required|min:11',
            'social_media' => 'max:100',
            'category' => 'required',
            'diary_price',
            'recreation',
            'extra',
            'room',
            'capacity',
            'sandwich',
            'delivery',
            'holiday',
            'christmas',
            'new_year',
            'owner_name',
            'mechanic'
            //'open_time',
            //'close_time',
        ];
    }
}