<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // necessario para evitar erro de email unico se o usuario nao quiser alterar o email
        $id = auth()->user()->id;

        return [
            'name' => 'required|string|max:255',
            'email' => "required|string|email|max:255|unique:users,email,{$id},id",
            'password' => 'max:12',
            'image' => 'mimes:jpg,jpeg,png,svg|max:2000',
        ];
    }
}
