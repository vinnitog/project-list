<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\AdvertisementImage;
use App\RecreationArea;
use App\ExtraItem;
use App\SandwichVariation;
use App\MechanicPart;

class Advertisement extends Model
{
    private $totalPages = 5;
    
    protected $fillable = [
        'user_id', 
        'description', 
        'open_time', 
        'close_time', 
        'email',
        'phone', 
        'price', 
        'social_media', 
        'category',
        'room',
        'capacity',
        'diary_price',
        'delivery',
        'holiday',
        'christmas',
        'new_year',
        'owner_name'
    ];

    public function searchUserAdvertisements(Array $data, $totalPages){
        
        // resgatando anuncios de acordo com o usuario logado
        $user_advertisements = auth()->user()->advertisements();

        //verifica se esta buscando por descricao ou por email e faz a consulta no banco
        return $user_advertisements->where(function ($query) use ($data){
            if (isset($data['owner_name'])){
                $query->where('owner_name', 'LIKE', '%'.$data['owner_name'].'%');
            }
            if (isset($data['descricao'])){
                $query->where('description', 'LIKE', '%'.$data['descricao'].'%');
            }
            if (isset($data['email'])){
                $query->where('email', 'LIKE', '%'.$data['email'].'%');
            }
            if (isset($data['category'])){
                $query->where('category', $data['category']);
            }
        })->paginate($totalPages);
        // outra forma de resgatar de acordo com usuario logado
        //->where('user_id', auth()->user()->id);

        //para debugar a consulta
        //->toSql(); 
    }

    public function searchAdvertisements(Array $data, $totalPages){
        
        //verifica se esta buscando por descricao ou por email e faz a consulta no banco
        return $this->where(function ($query) use ($data){
            if (isset($data['descricao'])){
                $query->where('description', 'LIKE', '%'.$data['descricao'].'%');
            }
            if (isset($data['email'])){
                $query->where('email', 'LIKE', '%'.$data['email'].'%');
            }
            if (isset($data['category'])){
                $query->where('category', $data['category']);
            }
        })->paginate($totalPages);
    }

    public function category($category = null){

        $categories = [
            'CHA' => 'Chácaras',
            'LAN' => 'Lanchonetes',
            'MEC' => 'Mecânicas',
            'SUP' => 'Supermercados',
        ];

        if(!$category){
            return $categories;
        }else{
            return $categories[$category];
        }
    }

    public function recreation_area($recreation_area = null){

        $recreation_areas = [
            'PISCINA' => 'Piscina',
            'CAMPO DE FUTEBOL' => 'Campo de Futebol',
            'QUADRA' => 'Quadra',
            'QUIOSQUE' => 'Quiosque',
        ];

        if(!$recreation_area){
            return $recreation_areas;
        }else{
            return $recreation_areas[$recreation_area];
        }
    }

    public function extra_item($extra_item = null){

        $extra_items = [
            'SINUCA' => 'Sinuca',
            'PEBOLIM' => 'Pebolim',
            'PING PONG' => 'Ping Pong',
        ];

        if(!$extra_item){
            return $extra_items;
        }else{
            return $extra_items[$extra_item];
        }
    }

    public function sandwich_variation($sandwich_variation = null){

        $sandwich_variations = [
            'VEGETARIANO' => 'Vegetariano',
            'VEGANO' => 'Vegano',
            'CACHORRO QUENTE' => 'Cachorro Quente',
            'HAMBURGUER' => 'Hamburguer',
            'PIZZA' => 'Pizza',
            'PANQUECA' => 'Panqueca',
            'ESFIHA' => 'Esfiha',
            'PORÇÕES' => 'Porções',
            'REFRIGERANTES' => 'Refrigerantes',
            'AÇAÍ' => 'Açaí',
        ];

        if(!$sandwich_variation){
            return $sandwich_variations;
        }else{
            return $sandwich_variations[$sandwich_variation];
        }
    }

    public function mechanic_part($mechanic_part = null){

        $mechanic_parts = [
            'ESCAPAMENTOS' => 'Escapamentos',
            'ELÉTRICA' => 'Elétrica',
            'FREIO' => 'Freio',
            'TROCA DE ÓLEO' => 'Troca de óleo',
            'PNEUS' => 'Pneus',
            'MOTORES' => 'Motores',
        ];

        if(!$mechanic_part){
            return $mechanic_parts;
        }else{
            return $mechanic_parts[$mechanic_part];
        }
    }

    public function advertisement_images(){
        return $this->hasMany(AdvertisementImage::class);
    }

    public function recreation_areas(){
        return $this->hasMany(RecreationArea::class);
    }

    public function extra_items(){
        return $this->hasMany(ExtraItem::class);
    }

    public function sandwich_variations(){
        return $this->hasMany(SandwichVariation::class);
    }

    public function mechanic_parts(){
        return $this->hasMany(MechanicPart::class);
    }
}
