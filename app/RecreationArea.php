<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecreationArea extends Model
{
    
    protected $fillable = [
        'advertisement_id', 'recreation_area'
    ];

    public function advertisements(){
        return $this->belongsTo(Advertisement::class);
    }
}
