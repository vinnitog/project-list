<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MechanicPart extends Model
{
    protected $fillable = [
        'advertisement_id', 'mechanic_part'
    ];

    public function advertisements(){
        return $this->belongsTo(Advertisement::class);
    }
}
