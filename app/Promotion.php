<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Promotion extends Model
{
    protected $fillable = [
        'user_id', 'image', 'price', 'name', 'status', 'phone', 'owner_name'
    ];

    //promotion pertence ao user
    public function user(){
        return $this->belongsTo(User::class);
    }
}
