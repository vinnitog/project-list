<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraItem extends Model
{
    protected $fillable = [
        'advertisement_id', 'extra_item'
    ];

    public function advertisements(){
        return $this->belongsTo(Advertisement::class);
    }
}
