<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCategoryAdvertisementsSup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE advertisements MODIFY COLUMN category ENUM('CHA', 'LAN', 'MEC', 'SUP')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE advertisements MODIFY COLUMN category ENUM('CHA', 'LAN', 'MEC')");
    }
}
