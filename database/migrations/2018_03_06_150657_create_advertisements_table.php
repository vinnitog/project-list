<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {
            
            $table->increments('id');
            // define como unsigned pois ira trabalhar o user_id como chave estrangeira
            $table->integer('user_id')->unsigned();
            // declarando user_id como chave estrangeira referenciando o campo id da tabela users e quando esse usuario for deletado todos os anuncios que pertencem a ele serao deletados tambem
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // atributos comun a todas categorias
            $table->longText('description');
            $table->string('email')->nullable();
            $table->string('phone');
            // o nullable demonstra que sera um valor opcional, nao sera obrigatorio
            $table->string('social_media')->nullable();
            
            //categorias
            $table->enum('category', array('CHA', 'LAN'));

            //campos/atributos de chacaras
            $table->integer('room')->nullable();
            $table->integer('capacity')->nullable();
            $table->decimal('diary_price', 10, 2)->nullable();

            //campos/atributos de lanchonetes
            $table->dateTime('open_time')->nullable();
            $table->dateTime('close_time')->nullable();
            $table->boolean('delivery')->nullable()->default(0);
            $table->boolean('holiday')->nullable()->default(0);
            $table->boolean('christmas')->nullable()->default(0);
            $table->boolean('new_year')->nullable()->default(0);

            // responsavel pelos campos created_at e updated_at
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements');
    }
}
