<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'vinni',
            'email'=>'vinnitog@gmail.com',
            
            // o bcrypt eh um metodo helper para criptografar a string
            'password'=>bcrypt('123321'),
        ]);

        User::create([
            'name'=>'vinni2',
            'email'=>'vinnitog2@gmail.com',
            
            // o bcrypt eh um metodo helper para criptografar a string
            'password'=>bcrypt('123123'),
        ]);
    }
}
